package week4.day1;

public class PercentageOfCharInString {

	public static void main(String[] args) {
		int countUpper = 0;
		int countLower = 0;
		int countWhiteSpace = 0;
		int countSpecial = 0;
		double percentage;

		String sentence = "Tiger Runs@ The Speed Of 100km/hour.";
		char charval[] = sentence.toCharArray();
		int length = charval.length;
		for (int i = 0; i < charval.length; i++) {
			if (Character.isUpperCase(charval[i]) || Character.isLowerCase(charval[i])) {
				{
					if (Character.isUpperCase(charval[i])) {
						countUpper++;
					} else {
						countLower++;

					}
				}

			} else if (Character.isWhitespace(charval[i]) || (!Character.isWhitespace(charval[i]))) {

				if (Character.isWhitespace(charval[i])) {
					countWhiteSpace++;
				} else {
					countSpecial++;

				}
			}
		}
		percentage = ((float) countUpper / length) * 100;
		System.out.println(countUpper);
		String percentageForUpper = String.format("%.2f", percentage);
		System.out.println("percentage For Uppercase " + percentageForUpper);
		percentage = ((float) countLower / length) * 100;
		System.out.println(countLower);
		String percentagecountLower = String.format("%.2f", percentage);
		System.out.println("percentage For Lowecase " + percentagecountLower);
		percentage = ((float) countWhiteSpace / length) * 100;
		System.out.println(countWhiteSpace);
		String perccountWhiteSpace = String.format("%.2f", percentage);
		System.out.println("percentage For WhiteSpace " + perccountWhiteSpace);
		percentage = ((float) countSpecial / length) * 100;
		String perccountSpecial= String.format("%.2f", percentage);
		System.out.println(perccountSpecial);

		System.out.println("percentage For Special " + perccountSpecial);

	}
}