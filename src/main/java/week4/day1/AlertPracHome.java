package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class AlertPracHome {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver chBrowser=new ChromeDriver(); 
		chBrowser.manage().window().maximize();
		chBrowser.get("https://www.quackit.com/javascript/tutorial/javascript_popup_boxes.cfm");
		//chBrowser.get("https://craftpip.github.io/jquery-confirm/");
		chBrowser.manage().timeouts().implicitlyWait(200,TimeUnit.SECONDS);
		//chBrowser.findElementByXPath("//button[text()='example confirm']").click();
		chBrowser.findElementByXPath("//input[@value='Display Example']").click();
		chBrowser.switchTo().alert().accept();
//		Thread.sleep(200);
		//chBrowser.findElementByXPath("//button[text()='confirm']").click();
		//chBrowser.findElementByXPath("//button[text()='ok']").click();

		//chBrowser.findElementByLinkText("ok").click();
		}
	
	//input[@value='Display Example']
}
