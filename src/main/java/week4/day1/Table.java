package week4.day1;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Table {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		//Create obj for chrome

		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		//Click the link 
		driver.findElementByLinkText("CRM/SFA").click();
		//Create leaf
		driver.findElementByLinkText("Create Lead").click();

		Thread.sleep(3000);
		driver.findElementByXPath("//img[@alt='View Calendar']").click();
		WebElement tableData = driver.findElementByXPath("//table/parent::div[@class='calendar']");
		List<WebElement> findTr = tableData.findElements(By.xpath("//tr[@class='daysrow']"));
/*		String secRow = findTr.get(1).getText();
		for (int j = 0; j < findTr.size(); j++) {
			
			String C = findTr.get(j).getText();
			//if(String.valueOf(C).contentEquals("7"))
			if(String.valueOf(C).contentEquals("7"))
			{
				findTr.get(j).click();		
			}
		}
		System.out.println("Below are the sencond row========" );
		System.out.println(secRow);
		System.out.println("Below are the All row========" );
		for (WebElement rowVal : findTr) {
			System.out.println(rowVal.getText());			
		}

		System.out.println("====================" );*/

		for (int i = 0; i < findTr.size(); i++) {
			List<WebElement> a = findTr.get(i).findElements(By.tagName("td"));
			for (int j = 0; j < findTr.size(); j++) {
				String b = a.get(j).getText();
				WebElement disabled = driver.findElementByXPath("//td[contains(@class,'day othermonth')]");
				if (String.valueOf(b).contentEquals("30") && disabled.isDisplayed()) {
					a.get(j).click();	
					System.out.println("select "+b+i);
			}
			
			}



		}
	}
}