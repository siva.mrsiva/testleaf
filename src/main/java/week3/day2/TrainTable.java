package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TrainTable {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		//Create obj for chrome

		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://erail.in/");
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("Mumbai Central",Keys.TAB);
		Thread.sleep(3000);
		WebElement ele = driver.findElementById("chkSelectDateOnly");
		// below if is used to avoid out of bound exception..check by comment below if
		if (ele.isSelected()) {
			ele.click();
		}
Thread.sleep(3000);
		WebElement tableData = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> findTr = tableData.findElements(By.tagName("tr"));
		String secRow = findTr.get(2).getText();
		System.out.println("Below are the sencond row========" );
		System.out.println(secRow);
		System.out.println("Below are the All row========" );
		for (WebElement rowVal : findTr) {
System.out.println(rowVal.getText());			
		}
		
		System.out.println("====================" );

		for (int i = 0; i < findTr.size(); i++) {
			List<WebElement> a = findTr.get(i).findElements(By.tagName("td"));
			String b = a.get(0).getText();
			System.out.println(b);
			
		}
	}

}
 