package week3.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class DupInArray {

	public static void main(String[] args) {
		int a[] = { 13, 65, 15, 67, 88, 65, 13, 99, 67, 13, 65, 87, 13 };

		Set<Integer> val = new HashSet<>();
		Set<Integer> b = new HashSet<>();

		for (Integer i = 0; i < a.length; i++)

		{

			if (val.add(a[i]) == false) {

				b.add(a[i]);
				// System.out.println(b);
			}

		}
		System.out.println("Value present in the Set=" + val);

		List<Integer> d = new ArrayList<>();
		d.addAll(b);
		Collections.reverse(d);
		for (Integer c : d) {
			System.out.print(" "+ c);
		}
		System.out.println("\n");
		Iterator<Integer> itVal=d.iterator();
		while(itVal.hasNext())
		{
			System.out.print("Using Itrator= "+itVal.next());
		}
	}

}
