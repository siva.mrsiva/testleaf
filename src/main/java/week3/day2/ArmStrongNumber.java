package week3.day2;

import java.util.Scanner;

public class ArmStrongNumber {

	public static void main(String[] args) {
		// int no = 153;
		// int no;
		// Scanner input=new Scanner(System.in);
		// no=input.nextInt();

		int sum = 0;
		for (int no = 100; no < 1000; no++) {

			int temp = no;
			while (temp != 0) {
				int remin = temp % 10;
				sum = sum + (remin * remin * remin);
				temp = temp / 10;

			}
			if (no == sum) {
				System.out.println(sum + " is Armstron number");
			} else {
				sum = 0;
			}
		}

	}
}