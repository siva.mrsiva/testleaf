package week3.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Prat {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		WebElement Prefferedlang = driver.findElementById("userRegistrationForm:countries");
		Select selprefer = new Select(Prefferedlang);
		selprefer.selectByValue("94");
		String val = driver.findElementById("userRegistrationForm:isdCode").getAttribute("value");
		//String val = driver.findElementById("userRegistrationForm:isdCode").getTagName();
		System.out.println(val);
	}

}
