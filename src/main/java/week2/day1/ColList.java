package week2.day1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.poi.ddf.EscherColorRef.SysIndexSource;

import week2.day2.Practice;

public class ColList extends Practice {

	public static void main(String[] args) {

		// String collection list
		List<String> al = new ArrayList<>();
		al.add("Siva");
		al.add("Anu");
		al.add("Sai Raksha");
		al.add("Sai Raksha");
		al.add("Sai Raksha");
		
		//int collection list 
		
		List<Integer> inobj= new ArrayList<>();
		
		inobj.add(10);
		inobj.add(20);
		inobj.add(30);
		System.out.println("---------------");
		System.out.println(inobj);
		
		Collections.reverse(inobj);
		System.out.println(inobj);
		Comparator<Integer> logic=new LogiImpl();
		Collections.sort(inobj,logic);
		System.out.println(inobj);
		
			
		//Set<String> hs=new HashSet<>();
		Set<String> hs=new TreeSet<>();
		hs.addAll(al);
		System.out.println("---------------");
		System.out.println(hs);
		int sum = 0;
		for (String outstr : hs) {

			System.out.println(outstr);
			/*if (outstr == "Sai Raksha") {

				sum++;
			}
*/		}
		hs.add("AAA");
		System.out.println("---------------");
		System.out.println(hs);

		//System.out.println(sum);
	}

}
