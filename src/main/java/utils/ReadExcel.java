package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	public static Object[][] fetchExcel() throws IOException {
		// To open the workbook
		XSSFWorkbook wb = new XSSFWorkbook("./data/createLead.xlsx");

		// Go to the sheet
		XSSFSheet sheetName = wb.getSheet("TestData");
		// Row count
		int lastRowNum = sheetName.getLastRowNum();
		System.out.println("No of Row present " + lastRowNum);
		
		// Cell or Column count
		int lastCellNum = sheetName.getRow(0).getLastCellNum();
		System.out.println("No of column Present" + lastCellNum);
     Object data[][]=new Object[lastRowNum][lastCellNum];
		for (int i = 1; i <= lastRowNum; i++) {
			XSSFRow row = sheetName.getRow(i);

			for (int j = 0; j < lastCellNum; j++) {
				XSSFCell cell = row.getCell(j);
				try {
					String stringCellValue = cell.getStringCellValue();
					System.out.println("Value of "+i+" "+j+" index =" + stringCellValue);
					data[i-1][j]=cell.getStringCellValue();
				} catch (NullPointerException e) {
					System.out.println("Null Pointer");
				}
				
			}
			
		}
		return data;

	}
}